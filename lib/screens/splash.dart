import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_11/screens/home.dart';
import 'package:google_fonts/google_fonts.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    return AnimatedSplashScreen(
      backgroundColor: Colors.black,
      splashIconSize: 350,
      splash: Column(
        children: [
          Container(
            height: 200,
            child: Image(
              image: AssetImage("assets/images/Alien.png"),
            ),
          ),
          Text.rich(TextSpan(
              text: "Alien",
              style: GoogleFonts.robotoMono(
                color: Colors.white70,
                fontSize: 50,
                fontWeight: FontWeight.bold,
              ),
              children: [TextSpan(text: "Ware.")]))
        ],
      
    ), nextScreen: Home());
    
  }
}
